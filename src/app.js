import * as THREE from 'three';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { MeshLine, MeshLineMaterial } from 'three.meshline';

import {
    initSides,
    initLights,
    onMouseMove,
    render,
} from './helpers';
import { scene } from './scene';
import { pickableMeshes } from './pickableMeshes';



const gltfLoader = new GLTFLoader();
initLights();

gltfLoader.load('https://raw.githubusercontent.com/lubuger/test_data/main/building.gltf', (gltf) => {
    const model = gltf.scene;

    model.rotation.x += 5;
    model.rotation.z = 0;
    model.rotation.y = 0;

    // get a list of all the meshes in the scene
    model.traverse((node) => {
        if (node instanceof THREE.Mesh) {
            pickableMeshes.push(node);

            const geo = new THREE.EdgesGeometry( node.geometry, 10 ); // EdgesGeometry or WireframeGeometry
            const position = geo.getAttribute("position").array;
            const wireframe = new MeshLine();
            const resolution = new THREE.Vector2( window.innerWidth, window.innerHeight );

            wireframe.setGeometry(position);

            const wireframeMesh = new THREE.Mesh(wireframe.geometry, new MeshLineMaterial({
                color: new THREE.Color(0x000000),
                // sizeAttenuation: true,
                // lineWidth: 0.02,

                map: null,
                useMap: false,
                // opacity: .5,
                resolution: resolution,
                // sizeAttenuation: false,
                lineWidth: 0.05,
                // depthWrite: false,
                // depthTest: false,
                // transparent: true
            }));

            // add to child so we get same orientation
            node.add(wireframeMesh);
            // to parent of child. Using attach keeps our orietation
            node.parent.attach(wireframeMesh);
            // remove child (we don't want child)
            // node.parent.remove(node);

            const material = node.material.clone();

            material.color.setHex( 0xffffff );

            node.material = material;
        }
    });

    scene.add(model);

    initSides();
});

requestAnimationFrame(render);

addEventListener( 'mousemove', onMouseMove, false );