import * as THREE from 'three';
import { scene } from './scene';
import { pickableMeshes } from './pickableMeshes';

import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';

const canvas = document.querySelector('#c');
const sides = document.querySelector('#sides');

const renderer = new THREE.WebGLRenderer({
    canvas,
    antialias: true
});
const camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
const raycaster = new THREE.Raycaster();
const controls = new OrbitControls(camera, canvas);
const mouse = new THREE.Vector2();

const intersected = [];
const fov = 45;
const aspect = 2;  // the canvas default
const near = 0.1;
const far = 100;

camera.position.set(-20, 15, 10);

controls.enablePan = false;
controls.target.set(5, 2, -10);
controls.update();

const mappings = [
    {
        name: 'VL',
        maps: [
            'LH15',
            'LH18',
            'LH19',
            'LH20',
        ]
    },
    {
        name: 'CH',
        maps: [
            'LH17',
            'LH10',
            'LH07',
            'LH08',
            'LH14',
        ]
    },
    {
        name: 'TH',
        maps: [
            'LH12',
            'LH13',
            'LH16',
            'LH11',
            'LH09',
            'LH00',
            'LH01',
            'LH02',
            'LH03',
        ]
    },
    {
        name: 'YH',
        maps: [
            'LH04',
            'LH05',
            'LH06',
        ]
    },
];


function initSides() {
    const sidesList = sides.querySelectorAll('div');

    for (const side of sidesList) {
        side.addEventListener('click', (e) => {
            const id = e.target.getAttribute('data-side');
            const found = mappings.filter(el => el.name === id);

            if (!found) {
                return false;
            }

            for (const mesh of pickableMeshes) {
                const material = mesh.material.clone();

                if (found[0].maps.includes(mesh.name)) {
                    if (material.color.getHexString() === 'ffffff') {
                        material.color.setHex( 0xA06E64 );
                    } else {
                        material.color.setHex( 0xffffff );
                    }
                } else {
                    material.color.setHex( 0xffffff );
                }

                mesh.material = material;
            }
        });
    }


    return true;
}

function initLights() {
    const skyColor = 0xFFFFFF;  // light blue
    const groundColor = 0xFFFFFF;  // brownish orange
    const lightHem = new THREE.HemisphereLight(skyColor, groundColor, intensity);

    scene.add(lightHem);

    const color = 0xFFFFFF;
    const intensity = 1;
    const lightDir = new THREE.DirectionalLight(color, intensity);

    lightDir.castShadow = false;
    lightDir.position.set(0, 0, 0);

    scene.add(lightDir);
    scene.add(lightDir.target);

    return true;
}

function intersect() {
    // update the picking ray with the camera and mouse position
    raycaster.setFromCamera( mouse, camera );

    // calculate objects intersecting the picking ray
    const intersects = raycaster.intersectObjects( pickableMeshes );

    if (intersects.length > 0) {
        clearIntersected();

        const target = intersects[ 0 ].object;
        const material = target.material.clone();

        material.color.setHex( 0xA06E64 );

        target.material = material;
        intersected[ 0 ] = intersects[ 0 ];

        document.body.style.cursor = 'pointer';

        return true;
    } else {
        clearIntersected();

        document.body.style.cursor = 'default';

        return true;
    }
}

function clearIntersected() {
    if (intersected.length > 0) {
        for ( let i = 0; i < intersected.length; i ++ ) {
            const target = intersected[ i ].object;
            const material = target.material.clone();

            material.color.setHex( 0xffffff );

            target.material = material;
        }

        intersected.splice(0, intersected.length);

        return true;
    }

    return false;
}

function getCanvasRelativePosition(event) {
    const rect = canvas.getBoundingClientRect();

    return {
        x: event.clientX - rect.left,
        y: event.clientY - rect.top,
    };
}

function onMouseMove(event) {
    const pos = getCanvasRelativePosition(event);
    // calculate mouse position in normalized device coordinates
    // (-1 to +1) for both components
    mouse.x = ( pos.x / window.innerWidth ) * 2 - 1;
    mouse.y = - ( pos.y / window.innerHeight ) * 2 + 1;

    return true;
}

function frameArea(sizeToFitOnScreen, boxSize, boxCenter, camera) {
    const halfSizeToFitOnScreen = sizeToFitOnScreen * 0.5;
    const halfFovY = THREE.MathUtils.degToRad(camera.fov * .5);
    const distance = halfSizeToFitOnScreen / Math.tan(halfFovY);
    // compute a unit vector that points in the direction the camera is now
    // in the xz plane from the center of the box
    const direction = (new THREE.Vector3())
        .subVectors(camera.position, boxCenter)
        .multiply(new THREE.Vector3(1, 0, 1))
        .normalize();

    // move the camera to a position distance units way from the center
    // in whatever direction the camera was from the center already
    camera.position.copy(direction.multiplyScalar(distance).add(boxCenter));

    // pick some near and far values for the frustum that
    // will contain the box.
    camera.near = boxSize / 100;
    camera.far = boxSize * 100;

    camera.updateProjectionMatrix();

    // point the camera to look at the center of the box
    camera.lookAt(boxCenter.x, boxCenter.y, boxCenter.z);

    return true;
}

function resizeRendererToDisplaySize(renderer) {
    const canvas = renderer.domElement;
    const width = canvas.clientWidth;
    const height = canvas.clientHeight;
    const needResize = canvas.width !== width || canvas.height !== height;

    if (needResize) {
        renderer.setSize(width, height, false);
    }

    return needResize;
}

function render() {
    if (resizeRendererToDisplaySize(renderer)) {
        const canvas = renderer.domElement;

        camera.aspect = canvas.clientWidth / canvas.clientHeight;
        camera.updateProjectionMatrix();
    }

    intersect();

    renderer.render(scene, camera);

    requestAnimationFrame(render);
}

export {
    initSides,
    initLights,
    intersect,
    clearIntersected,
    getCanvasRelativePosition,
    onMouseMove,
    frameArea,
    resizeRendererToDisplaySize,
    render,

    near,
    far,
}