import * as THREE from 'three';

const scene = new THREE.Scene();

scene.background = new THREE.Color('#f0f4f0');

export {
    scene,
}